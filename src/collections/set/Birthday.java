package collections.set;

import org.junit.Test;

import java.util.*;


public class Birthday {

    @Test
    public void runCode() {
        List<Integer> sameDay = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            sameDay.add(extracted());

        }
        int sum = 0;
        for (Integer i : sameDay) {
            sum += i;
        }
        System.out.println(sum / sameDay.size());


    }

    private int extracted() {
        Random r = new Random();
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < 365; i++) {
            int randomDayOfYear = r.nextInt(365);
            if (set.contains(randomDayOfYear)) {
                return i;
            } else {
                set.add(randomDayOfYear);
            }
        }
        throw new IllegalStateException("Should not be here!");
    }

}
