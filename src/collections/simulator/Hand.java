package collections.simulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Hand implements Iterable<Card>, Comparable<Hand> {

    private final List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        if (straightFlush()) {
            return HandType.STRAIGHT_FLUSH;
        } else if (fourOfAKind()) {
            return HandType.FOUR_OF_A_KIND;
        } else if (fullHouse()) {
            return HandType.FULL_HOUSE;
        } else if (regularFlush()) {
            return HandType.FLUSH;
        } else if (straight()) {
            return HandType.STRAIGHT;
        } else if (trips()) {
            return HandType.TRIPS;
        } else if (twoPairs()) {
            return HandType.TWO_PAIRS;
        } else if (onePair()) {
            return HandType.ONE_PAIR;
        } else {
            return HandType.HIGH_CARD; }
    }

    private boolean regularFlush() {
        Card.CardSuit suit = cards.get(0).getSuit();
        if (suit == cards.get(1).getSuit() && suit == cards.get(2).getSuit() && suit == cards.get(3).getSuit() && suit == cards.get(4).getSuit()) {
            return cards.get(4).getValue().ordinal() - cards.get(0).getValue().ordinal() != 4;
        }
        return false;
    }

    private boolean straightFlush() {
        Card.CardSuit suit = cards.get(0).getSuit();
        if (suit == cards.get(1).getSuit() && suit == cards.get(2).getSuit() && suit == cards.get(3).getSuit() && suit == cards.get(4).getSuit()) {
            return cards.get(4).getValue().ordinal() - cards.get(0).getValue().ordinal() == 4;
        }
        return false;
    }

    private boolean fourOfAKind () {
        int ordinalSum = 0;
        if (cards.size() == 4) {
            for (Card card: cards) {
                ordinalSum += card.getValue().ordinal();
            }
            return ordinalSum / 4 == cards.get(0).getValue().ordinal();
        } if (cards.size() == 5) {
            Card.CardValue cardValue = cards.get(0).getValue();
            return cardValue == cards.get(1).getValue() && cardValue == cards.get(2).getValue() && cardValue == cards.get(3).getValue();
        }
        return false;
    }

    private boolean fullHouse() {
        ArrayList<Card.CardValue> suitsInHand = new ArrayList<>();
        for (Card card : cards) {
            if (!suitsInHand.contains(card.getValue())) {
                suitsInHand.add(card.getValue());
            }
        }
        return suitsInHand.size() == 2 && cards.size() == 5;
    }

    private boolean straight() {
        if (cards.size() == 5) {
            if (cards.get(0).getValue().ordinal() + cards.get(4).getValue().ordinal() == 15) {
                return true;
            } return cards.get(4).getValue().ordinal() - cards.get(0).getValue().ordinal() == 4;
        }
        return false;
    }

    private boolean trips() {
        int counter = 0;
        Card.CardValue firstCard = cards.get(0).getValue();
        for (Card card : cards) {
            if (card.getValue() == firstCard) {
                counter += 1;
            }
        }
        return counter == 3;
    }

    private boolean twoPairs() {
        ArrayList<Card.CardValue> suitsInHand = new ArrayList<>();
        for (Card card : cards) {
            if (!suitsInHand.contains(card.getValue())) {
                suitsInHand.add(card.getValue());
            }
        }
        return suitsInHand.size() >= 2 && cards.size() == 4;
    }

    private boolean onePair() {
        ArrayList<Card.CardValue> suitsInHand = new ArrayList<>();
        for (Card card : cards) {
            if (suitsInHand.contains(card.getValue())) {
                return true;
            } else {
                suitsInHand.add(card.getValue());
            }
        }
        return false;
    }


























    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    @Override
    public int compareTo(Hand other) {
        System.out.println(other.cards);
        System.out.println(cards);
        return 0;
    }
}
