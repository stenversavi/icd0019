package collections.streaks;

import java.util.ArrayList;
import java.util.List;

public class Code {

    public static List<List<String>> getStreakList(String input) {
        List<List<String>> streakList = new ArrayList<>();
        List<String> streak = new ArrayList<>();
        int indexCounter = 0;
        for (String letter : input.split("")) {
            indexCounter++;
            if (input.length() == 1) {
                streak.add(letter);
                streakList.add(streak);
            } else if (streak.size() == 0) {
                streak.add(letter);
            } else if (streak.contains(letter)) {
                streak.add(letter);
            } else {
                streakList.add(streak);
                streak = new ArrayList<>();
                streak.add(letter);
                if (indexCounter == input.length()) {
                    streakList.add(streak);
                }
                }

            }
        return streakList;
    }
}

