package exceptions.numbers;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

public class NumberConverter {

    public String language;
    private Properties property;

    public NumberConverter(String lang) {
        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);
        this.language = lang;
        Properties properties = new Properties();
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, StandardCharsets.UTF_8);

            properties.load(reader);
            this.property = properties;
        } catch (Exception e) {
            if (Objects.equals(this.language, "ru")) {
                throw new MissingLanguageFileException(language, e);
            } else if (Objects.equals(this.language, "fr")) {
                throw new BrokenLanguageFileException(language, e);
            } else if (Objects.equals(this.language, "es")) {
                throw new MissingTranslationException("No file");
            }

        } finally {
            close(is);
        }
    }

    public String numberInWords(Integer number) {
        if (Objects.equals(language, "es")) {
            throw new MissingTranslationException("No file.");
        }
        Properties content = property;
        if (content.containsKey(String.valueOf(number))) {
            return content.getProperty(String.valueOf(number));
        }
        int ones;
        if (11 <= number && number <= 19) {
            ones = number % 10;
            return content.getProperty(String.valueOf(ones)) + content.getProperty("teen");
        }
        if (20 <= number && number <= 99) {
            return overTwenty(number, content);
        }

        if (number >= 100) {
                return overHundred(number, content);
            }
        return content.getProperty(String.valueOf(number));
    }

    public String overTwenty(Integer number, Properties content) {
        int ones;
        ones = number % 10;
        if (number % 10 == 0) {
            return content.getProperty(String.valueOf(number / 10)) + content.getProperty("tens-suffix");
        }
        if (content.containsKey(String.valueOf((number / 10) * 10)) && ones != 0) {
            return content.getProperty(String.valueOf((number / 10) * 10)) + content.getProperty("tens-after-delimiter") + content.getProperty(String.valueOf(ones));
        }
        else {
            return content.getProperty(String.valueOf(number / 10)) + content.getProperty("tens-suffix") + content.getProperty("tens-after-delimiter") + content.getProperty(String.valueOf(ones));
        }
    }


    public String overHundred(Integer number, Properties content) {
        if (number % 100 == 0) {
            return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred");
        }
        else if (number % 10 == 0) {
            if (content.containsKey(String.valueOf(number % 100))) {
                return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf(number % 100));
            } else {
                return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf((number % 100) / 10)) + content.getProperty("tens-suffix");
            }
        }
        else if (number % 100 <= 10) {
            return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf(number % 100));
        }
        else if ((number % 100) >= 11 && 19 >= (number % 100)) {
            if (content.containsKey(String.valueOf(number % 100))) {
                return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf(number % 100));
            } else {
                return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf(number % 10)) + content.getProperty("teen");
            }
        }
        else if (content.containsKey(String.valueOf(number % 100 / 10 * 10))) {
            return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf(number % 100 / 10 * 10)) + content.getProperty("tens-after-delimiter") + content.getProperty(String.valueOf(number % 10));
        } else {
            return content.getProperty(String.valueOf(number / 100)) + content.getProperty("hundreds-before-delimiter") + content.getProperty("hundred") + content.getProperty("hundreds-after-delimiter") + content.getProperty(String.valueOf(number % 100 / 10)) + content.getProperty("tens-suffix") + content.getProperty("tens-after-delimiter") + content.getProperty(String.valueOf(number % 10));
        }
    }


    private static void close(FileInputStream is) {
        if (is == null) {
            return;
        }

        try {
            is.close();
        } catch (IOException ignore) {}
    }

}
