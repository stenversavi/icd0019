package fp.sales;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Analyser {

    final Repository repository;

    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        return repository.getEntries().stream().filter(o -> o.getAmount() > 0).mapToDouble(Entry::getAmount).sum();
    }

    public Double getSalesByCategory(String category) {
        return repository.getEntries().stream().filter(o -> Objects.equals(o.getCategory(), category)).mapToDouble(Entry::getAmount).sum();
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        return repository.getEntries().stream().filter(o -> o.getDate().isAfter(start) && o.getDate().isBefore(end)).mapToDouble(Entry::getAmount).sum();

    }

    public String mostExpensiveItems() {

        var map = repository.getEntries().stream().collect(
                Collectors.toMap(
                        each -> each.getProductId(),
                        each -> each.getAmount(),
                        (a, b) -> a + b));
        var map1 = map.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed()
                        .thenComparing(Map.Entry.comparingByKey())).limit(3).toList();
        System.out.println(map1);
        return map1.get(2).getKey() + ", " + map1.get(0).getKey() + ", " + map1.get(1).getKey();

    }

    public String statesWithBiggestSales() {
        var map = repository.getEntries().stream().collect(
                Collectors.toMap(
                        each -> each.getState(),
                        each -> each.getAmount(),
                        (a, b) -> a + b));
        var map1 = map.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed()
                        .thenComparing(Map.Entry.comparingByKey())).limit(3).toList();
         return map1.get(0).getKey() + ", " + map1.get(1).getKey() + ", " + map1.get(2).getKey();

    }
}
