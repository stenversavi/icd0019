package fp.sales;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    public List<Entry> getEntries() {
        File file = new File(FILE_PATH);
        try {
            InputStream is = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            return br.lines().skip(1)
                    .map(line -> convert(line))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public Entry convert(String line) {
        Entry entry = new Entry();
        String[] newLine = line.split("\t");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        entry.setDate(LocalDate.parse(newLine[0], formatter));
        entry.setState(newLine[1]);
        entry.setProductId(newLine[2]);
        entry.setCategory(newLine[3]);
        entry.setAmount(Double.valueOf(newLine[5].replaceAll(",", ".")));
        return entry;
    }

}
