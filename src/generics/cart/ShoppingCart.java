package generics.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ShoppingCart<T extends CartItem> {
    private final List<CartItem> results = new ArrayList<>();
    private double discountPercentage;
    private double lastDiscount;

    public String toString() {
        List<CartItem> usedItems = new ArrayList<>();
        StringBuilder intoString = new StringBuilder();
        int checkedItems = 0;
        for (CartItem item : results) {
            int itemStock = 0;
            for (CartItem item1 : results) {
                if (usedItems.contains(item)) {
                    break;
                } else if (Objects.equals(item.getId(), item1.getId())){
                    itemStock += 1;
                    checkedItems += 1;
                }
            }
            if (checkedItems == results.size()) {
                intoString.append("(").append(item.getId()).append(", ").append(item.getPrice()).append(", ").append(itemStock).append(")");
                return intoString.toString();
            }
            intoString.append("(").append(item.getId()).append(", ").append(item.getPrice()).append(", ").append(itemStock).append("), ");
            usedItems.add(item);
        }
        return intoString.toString();
    }


    public void add(CartItem item) {
        results.add(item);
    }

    public void removeById(String id) {
        results.removeIf(item -> Objects.equals(item.getId(), id));
    }

    public Double getTotal() {
        double totalSum = 0;
        for (CartItem item : results) {
            totalSum += item.getPrice();
        }
        if (discountPercentage > 0) {
            return totalSum * discountPercentage;
        }
        return totalSum;
    }

    public void increaseQuantity(String id) {
        for (CartItem item : results) {
            if (Objects.equals(item.getId(), id)) {
                results.add(item);
                break;
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        if (discountPercentage > 0) {
            discountPercentage = discountPercentage * (1 - discount / 100);
        } else {
            discountPercentage = 1 * (1 - discount / 100);
        }
        lastDiscount = discount / 100;

    }

    public void removeLastDiscount() {
        discountPercentage = discountPercentage / (1 - lastDiscount);
    }

    public void addAll(List<? extends CartItem> items) {
        results.addAll(items);
    }
}
