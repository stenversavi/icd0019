package gol;

public class Game {

    private boolean[][] board = new boolean[21][16];

    public void markAlive(int x, int y) {
        board[y][x] = true;
    }

    public boolean isAlive(int x, int y) {
        return board[y][x];
    }

    public void toggle(int x, int y) {
        board[y][x] = !board[y][x];
    }

    public Integer getNeighbourCount(int x, int y) {
        if (x == 15 && y == 0) {
            return neighbourCountLeftDown(x, y);
        }
        if (x == 0 && y > 0) {
            return neighbourCountUp(x, y);
        }
        if (x == 0 && y == 0) {
            return neighbourCountLeftUp(x, y);
        }
        if (x > 0 && x < 15 && y == 0) {
            return neighbourCountLeft(x, y);
        }
        else  {
            return getAllNeighbours(x, y);
        }

    }


    public void nextFrame() {
        boolean[][] temporaryBoard = new boolean[21][16];
        for (int row = 0; row < board.length - 1; row++) {
            for (int col = 0; col < board[row].length - 1; col++) {
                temporaryBoard[row][col] = nextState(board[row][col], getNeighbourCount(col, row));
            }
        }
        board = temporaryBoard;
    }

    public void clear() {
        for (int row = 0; row < board.length - 1; row++) {
            for (int col = 0; col < board[row].length - 1; col++) {
                board[row][col] = false;
            }
        }

    }

    public boolean nextState(boolean isLiving, int neighborCount) {
        if (isLiving) {
            return neighborCount == 2 || neighborCount == 3;
        } else {
            return neighborCount == 3;
        }
    }

    public int getAllNeighbours(int x, int y) {
        int neighbourCount = 0;
        if (board[y - 1][x]) {
            neighbourCount += 1;
        } if (board[y + 1][x]) {
            neighbourCount += 1;
        } if (board[y][x - 1]) {
            neighbourCount += 1;
        } if (board[y][x + 1]) {
            neighbourCount += 1;
        }
        neighbourCount += neighbourHelper(x, y);
        return neighbourCount;

    }
    public int neighbourHelper(int x, int y) {
        int neighbourCount = 0;
        if (board[y - 1][x - 1]) {
            neighbourCount += 1;
        } if (board[y + 1][x - 1]) {
            neighbourCount += 1;
        } if (board[y - 1][x + 1]) {
            neighbourCount += 1;
        } if (board[y + 1][x + 1]) {
            neighbourCount += 1;
        }
        return neighbourCount;
    }

    public int neighbourCountLeft(int x, int y) {
        int neighbourCount = neighbourCount(x, y);
        if (board[y + 1][x - 1]) {
            neighbourCount += 1;
        } if (board[y][x - 1]) {
            neighbourCount += 1;
        }
        return neighbourCount;
    }

    private int neighbourCount(int x, int y) {
        int neighbourCount = 0;
        if (board[y][x + 1]) {
            neighbourCount += 1;
        } if (board[y + 1][x]) {
            neighbourCount += 1;
        } if (board[y + 1][x + 1]) {
            neighbourCount += 1;
        }
        return neighbourCount;
    }

    public int neighbourCountLeftUp(int x, int y) {
        return neighbourCount(x, y);
    }

    public int neighbourCountLeftDown(int x, int y) {
        int neighbourCount = 0;
        if (board[y + 1][x]) {
            neighbourCount += 1;
        } if (board[y][x - 1]) {
            neighbourCount += 1;
        } if (board[y + 1][x - 1]) {
            neighbourCount += 1;
        }
        return neighbourCount;
    }

    public int neighbourCountUp(int x, int y) {
        int neighbourCount = 0;
        if (board[y][x + 1]) {
            neighbourCount += 1;
        } if (board[y + 1][x]) {
            neighbourCount += 1;
        } if (board[y - 1][x]) {
            neighbourCount += 1;
        } if (board[y + 1][x + 1]) {
            neighbourCount += 1;
        } if (board[y - 1][x + 1]) {
            neighbourCount += 1;
        }
        return neighbourCount;
    }
}
