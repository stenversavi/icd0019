package inheritance.analyser;

import java.util.List;
import java.util.Objects;

public sealed abstract class AbstractSalesAnalyser permits DifferentiatedTaxSalesAnalyser, FlatTaxSalesAnalyser, TaxFreeSalesAnalyser {
    protected final List<SalesRecord> records;

    public AbstractSalesAnalyser(List<SalesRecord> records) {
        this.records = records;
    }

    protected abstract Double getTotalSales();

    protected abstract Double getTotalSalesByProductId(String id);

    protected String getIdOfMostPopularItem() {
        int mostItems = 0;
        String popularItem = "";
        for (SalesRecord record : records) {
            if (getTotalSalesByProductId(record.getProductId()) > mostItems) {
                mostItems = record.getItemsSold();
                popularItem = record.getProductId();
            }

        }
        return popularItem;
    }

    protected String getIdOfItemWithLargestTotalSales() {
        int salesProfit = 0;
        String popularItem = "";
        for (SalesRecord record : records) {
            if (Objects.equals(popularItem, record.getProductId())) {
                salesProfit += record.getProductPrice() * record.getItemsSold();
            } else if (record.getProductPrice() * record.getItemsSold() > salesProfit) {
                salesProfit = record.getProductPrice() * record.getItemsSold();
                popularItem = record.getProductId();

            }

        }
        return popularItem;
    }
}