package inheritance.analyser;

import java.util.List;
import java.util.Objects;

public non-sealed class DifferentiatedTaxSalesAnalyser extends AbstractSalesAnalyser {


    protected DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    protected Double getTotalSales() {
        double totalSales = 0;
        for (SalesRecord record : records) {
            if (record.hasReducedRate()) {
                totalSales += record.getProductPrice() / 1.1 * record.getItemsSold();
            } else {
                totalSales += record.getProductPrice() / 1.2 * record.getItemsSold();
            }
        }
        return totalSales;
    }

    protected Double getTotalSalesByProductId(String id) {
        double totalSales = 0;
        for (SalesRecord record : records) {
            if (Objects.equals(record.getProductId(), id)) {
                if (record.hasReducedRate()) {
                    totalSales += record.getItemsSold() / 1.1;
                }
                totalSales += record.getItemsSold() / 1.2;
            }
        }
        return totalSales;
    }

}

