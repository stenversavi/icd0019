package inheritance.analyser;

import java.util.List;
import java.util.Objects;

public non-sealed class FlatTaxSalesAnalyser extends AbstractSalesAnalyser {

    protected FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }
    @Override
    protected Double getTotalSales() {
        double totalSales = 0;
        for (SalesRecord i : records) {
            totalSales += i.getProductPrice() * i.getItemsSold();
        }
        return totalSales / 1.2;
    }

    @Override
    protected Double getTotalSalesByProductId(String id) {
        double totalSales = 0;
        for (SalesRecord record : records) {
            if (Objects.equals(record.getProductId(), id)) {
                totalSales += record.getItemsSold();
            }
        }
        return totalSales / 1.2;
    }


}