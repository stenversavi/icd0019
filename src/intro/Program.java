package intro;

public class Program {

    public static void main(String[] args) {
        int decimal = asDecimal("11001101");
        System.out.println(decimal); // 205
        System.out.println(asString(205));
    }


    public static String asString(int input) {
        // 10 = 1010
        String result = "";
        while (input > 0) {
            if (input % 2 == 0) {
                result = '0' + result;
            } else {
                result = '1' + result;
            }
            input /= 2;
        }
        return result;
    }

    public static int asDecimal(String input) {
        //11001101
        input = reverse(input);
        int result = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '1') {
                result += pow(2, i);
            }
        }
        return result;
    }

    private static String reverse(String input) {
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            result = input.charAt(i) + result;
        }
        return result;
    }

    private static int pow(int arg, int power) {
        // Java has Math.pow() but this time write your own implementation.
        int result = 1;
        if (arg == 0) {
            return 1;
        }
        while (power != 0) {
            result = result * arg;
            power--;
        }
        return result;
    }
}
