package junit;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Code {

    public static boolean isSpecial(int candidate) {
        return candidate % 11 == 0 || candidate % 11 == 1 || candidate % 11 == 2 || candidate % 11 == 3;
    }

    public static int longestStreak(String inputString) {
        if (inputString.length() == 0) {
            return 0;
        }
        char[] characters = inputString.toCharArray();
        int maxCount = 1;
        int counter = 1;
        for (int i = 0; i < characters.length - 1; i++) {
            if (characters[i] == characters[i + 1]) {
                counter += 1;
                if (counter > maxCount) {
                    maxCount += 1;
                }
            } else {
                counter = 1;
            }
        }
        return maxCount;
    }

    public static Character mode(String inputString) {
        if (inputString == null) {
            return null;
        }
        int modeCount = 0;
        Character mode = null;
        for (char ch : inputString.toCharArray()) {
            int count = 0;
            for (char ch1 : inputString.toCharArray()) {
                if (ch == ch1) {
                    count++;
                }
            }
            if (count > modeCount) {
                modeCount = count;
                mode = ch;
            }
        }
        return mode;
    }

    public static int getCharacterCount(String allCharacters, char targetCharacter) {
        char[] characters = new char[allCharacters.length()];
        for (int i = 0; i < allCharacters.length(); i++) {
            characters[i] = allCharacters.charAt(i);
        }
        int charCount = 0;
        for (char ch : characters) {
            if (ch == targetCharacter) {
                charCount += 1;
            }
        }
        return charCount;
    }

    public static int[] removeDuplicates(int[] integers) {
        int[] noDups = new int[integers.length];
        int integerCount = 0;
        int zeroCounter = 0;
        for (int integer : integers) {
            int inOrNot = 0;
            for (int nr : noDups) {
                if (integer == nr && integer != 0) {
                    inOrNot = 1;
                    break; }
                else if (integer == 0) {
                    if (zeroCounter == 0) {
                        zeroCounter += 1;
                        noDups[integerCount] = integer;
                        integerCount += 1;
                        zeroCounter += 1;
                    }
                    break;
                }
            }
            if (inOrNot == 0 && integer != 0) {
                noDups[integerCount] = integer;
                integerCount += 1;
            }
        }
        return helper(noDups, integerCount);
    }

    public static int[] helper(int[] array, int length) {
        int[] finalArray = new int[length];
        for (int i = 0; i < finalArray.length; i++) {
            finalArray[i] = array[i];
        }
        return finalArray;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int totalSum = 0;
        for (int nr : removeDuplicates(integers)) {
            totalSum += nr;
        }
        return totalSum;
    }}
