package junit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("PMD")
public class Tests {

    @Test
    public void equalityExamples() {
        assertThat(1 + 2, is(3));
    }

    @Test
    public void assertThatAndAssertEqualsExample() {

    }

    @Test
    public void getCharacterCount() {
        assertThat(Code.getCharacterCount("abcdeefg", 'i'), is(0));
        assertThat(Code.getCharacterCount("abcdeefg", 'e'), is(2));
        assertThat(Code.getCharacterCount("abcdeefg", 'd'), is(1));
    }

    @Test
    public void findsSpecialNumbers() {
        assertTrue(Code.isSpecial(0));
        assertThat(Code.isSpecial(0), is(true));
        assertThat(Code.isSpecial(1), is(true));
        assertThat(Code.isSpecial(2), is(true));
        assertThat(Code.isSpecial(3), is(true));
        assertThat(Code.isSpecial(4), is(false));
        assertThat(Code.isSpecial(11), is(true));
        assertThat(Code.isSpecial(15), is(false));
        assertThat(Code.isSpecial(36), is(true));
        assertThat(Code.isSpecial(37), is(false));
        // other test cases for isSpecial() method
    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak(""), is(0));
        assertThat(Code.longestStreak("a"), is(1));
        assertThat(Code.longestStreak("abbcccaaaad"), is(4));
    }

    @Test
    public void findsModeFromCharactersInString() {
        assertThat(Code.mode(null), is(nullValue()));
        assertThat(Code.mode(""), is(nullValue()));
        assertThat(Code.mode("abcb"), is('b'));
        assertThat(Code.mode("cbbc"), is('c'));
    }

    @Test
    public void removesDuplicates() {
        assertThat(Code.removeDuplicates(arrayOf(0, 1)), is(arrayOf(0, 1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 3, 2)), is(arrayOf(1, 2, 3)));
        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
        assertThat(Code.removeDuplicates(arrayOf(100, 3, 100, 0, 4, 0, 562, 4)),
                is(arrayOf(100, 3, 0, 4, 562)));
        assertThat(Code.removeDuplicates(arrayOf(100, 0, 3, 100, 0, 4, 562, 4)),
                is(arrayOf(100, 0, 3, 4, 562)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 1)), is(1));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 3, 2)), is(6));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private int[] arrayOf(int... numbers) {
        return numbers;
    }

}