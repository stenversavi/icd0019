package junit.sales;



public class TopSalesFinder {

    private int counter;

    private SalesRecord[] sales = new SalesRecord[10];

    public void registerSale(SalesRecord record) {

        if (counter < sales.length) {
            sales[counter] = record;
            counter += 1;

        } else {
            sales = getNewAndBiggerArray(sales, record);
        }
    }

    public SalesRecord[] getNewAndBiggerArray(SalesRecord[] salesRecords, SalesRecord record) {
        SalesRecord[] biggerSalesRecords = new SalesRecord[salesRecords.length * 2];
        int counter1 = 0;
        for (SalesRecord rec : salesRecords) {
            biggerSalesRecords[counter1] = rec;
            counter1 += 1;
        }
        biggerSalesRecords[counter1] = record;
        counter += 1;
        return biggerSalesRecords;
    }

    public String[] findItemsSoldOver(int amount) {
        int stringArrayLength = 0;
        for (SalesRecord record : getSameProducts()) {
            if (record != null && record.getProductPrice() > amount) {
                stringArrayLength += 1;
            }
        }
        String[] finalSales = new String[stringArrayLength];
        int recordIndex = 0;
        for (SalesRecord record : getSameProducts()) {
            if (record != null && record.getProductPrice() > amount) {
                finalSales[recordIndex] = record.getProductId();
                recordIndex += 1;
            }
        }


        return finalSales;
    }


    public SalesRecord[] getSameProducts() {
        SalesRecord[] noDups = new SalesRecord[sales.length];
        int count = 0;

        for (SalesRecord sale : sales) {
            if (sale != null) {
                int productPrice = 0;
                int itemsSold = 0;
                for (SalesRecord s : sales) {
                    if (s != null && sale.getProductId().equals(s.getProductId())) {
                        productPrice += s.getProductPrice() * s.getItemsSold();
                        itemsSold += s.getItemsSold();
                    }
                }
                if (inOrNot(noDups, sale.getProductId())) {
                    noDups[count] = new SalesRecord(sale.getProductId(), productPrice, itemsSold);
                    count += 1;
            }}
        }
        return noDups;
    }

    public boolean inOrNot(SalesRecord[] records, String id) {

        for (SalesRecord record : records) {
            if (record != null && record.getProductId().equals(id)) {
                return false;
            }
        }
        return true;
    }


}


