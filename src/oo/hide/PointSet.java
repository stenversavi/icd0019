package oo.hide;

import java.util.Arrays;
import java.util.Objects;

public class PointSet {

    private Point[] points;

    private int filledArray;

    public PointSet(int capacity) {
        points = new Point[capacity];
    }

    public PointSet() {
        this(10);
    }

    public void add(Point point) {
        if (filledArray == points.length) {
            PointSet extendedPoints = new PointSet(points.length * 2);
            for (Point p : points) {
                extendedPoints.add(p);
            }
            extendedPoints.add(point);
            points = extendedPoints.points;
        }
        else {
            for (Point p : points) {
                if (Objects.equals(p, point)) {
                    break;
                }
                if (p == null) {
                    points[filledArray] = point;
                    filledArray += 1;
                    break;
                }
        }}}

    public int size() {
        int length = 0;
        for (Point el : points) {
            if (el != null) {
                length += 1;
            } else {
                return length;
            }
        }
        return length;
    }

    public boolean contains(Point point) {
        for (Point p : points) {
             if (Objects.equals(point, p)) {
                 return true;
             }
        }
        return false;
    }

    public String toString() {
        int size = size() - 1;
        String finalString = "";
        int indexCounter = 0;
        for (Point p : points) {
            if (indexCounter == size) {
                finalString += String.format("%s", p);
                return finalString;
            }
            if (p != null) {
            finalString += String.format("%s, ", p);
            indexCounter += 1;
            }
        }
        return finalString;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PointSet other)) {
            return false;
        }
        if (points.length != other.points.length) {
            return false;
        }
        int counter = 0;
        for (Point p : points) {
            for (Point po : other.points) {
                if (p != null && Objects.equals(p, po)) {
                    counter += 1;

                }
            }
            if (counter == size()) {
                return true;
            }
        }
        return false;
    }


    public PointSet subtract(PointSet other) {
        PointSet set = new PointSet();
        for (Point i : points) {
            int counter = 0;
            for (Point l : other.points) {
                counter += 1;
                if (Objects.equals(l, i)) {
                    break; }
                if (other.points.length == counter) {
                    set.add(i);
                }
                }
            }
        return set;
    }

    public PointSet intersect(PointSet other) {
        PointSet set2 = new PointSet();
        for (Point i : points) {
            for (Point l : other.points) {
                if (Objects.equals(i, l)) {
                    set2.add(l);
                }
            }
        }
        return set2;
    }
}
