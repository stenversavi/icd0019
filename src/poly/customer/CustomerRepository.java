package poly.customer;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";

    private List<AbstractCustomer> customers = new ArrayList<>();

    public Optional<AbstractCustomer> getCustomerById(String id) {

        fileOpening();
        for (AbstractCustomer customer : customers) {
            if (customer.id.equals(id)) {
                return Optional.of(customer);
            }
        }
        return Optional.empty();
    }


    public void remove(String id) {

        customers.removeIf(customer -> customer.id.equals(id));
        overWrite();
    }

    public void save(AbstractCustomer customer) {
        if (!(customers.contains(customer))) {
            fileOpening();
            customers.add(customer);
            overWrite();
        } else {
            StringBuilder fileContent = new StringBuilder();

            for (AbstractCustomer c : customers) {
                if (c.id.equals(customer.id)) {
                    fileContent.append(customer.asString());
                }
                else if (c.getClass().toString().contains("Regular")) {
                    fileContent.append(c.asString());
                } else if (c.getClass().toString().contains("Gold")) {
                    fileContent.append(c.asString());
                }
            }
            try {
                FileWriter f2 = new FileWriter(FILE_PATH, false);
                f2.write(String.valueOf(fileContent));
                f2.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public int getCustomerCount() {
        return customers.size();
    }

    private void overWrite() {
        StringBuilder fileContent = new StringBuilder();

        for (AbstractCustomer c : customers) {
            if (c.getClass().toString().contains("Regular")) {
                fileContent.append(c.asString());
            } if (c.getClass().toString().contains("Gold")) {
                fileContent.append(c.asString());
            }
        }
        try {
            FileWriter f2 = new FileWriter(FILE_PATH, false);
            f2.write(String.valueOf(fileContent));
            f2.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void fileOpening() {
        List<String> lines;
        try {
            lines = Files.readAllLines(
                    Paths.get(FILE_PATH));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        customers.clear();
        for (String line : lines) {
            String[] parts = line.split(";");
            String type = parts[0];
            if (Objects.equals(type, "REGULAR")) {
                customers.add(new RegularCustomer(parts[1], parts[2], Integer.parseInt(parts[3]), LocalDate.parse(parts[4], formatter)));

            }
            if (Objects.equals(type, "GOLD")) {
                customers.add(new GoldCustomer(parts[1], parts[2], Integer.parseInt(parts[3])));
            }
        }
    }


}
