package poly.customer;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Objects;

public final class RegularCustomer extends AbstractCustomer {

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);
        this.lastOrderDate = lastOrderDate;

    }


    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() >= 100) {
            Duration diff = Duration.between(lastOrderDate.atStartOfDay(), order.getDate().atStartOfDay());
            long diffDays = diff.toDays();
            if (diffDays <= 31) {
                bonusPoints += order.getTotal() * 1.5;
            } else {
                bonusPoints += order.getTotal();
            }
        }
    }

    @Override
    public String asString() {
        return String.format("REGULAR;%s;%s;%s;%s\n", id, name, bonusPoints, lastOrderDate);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractCustomer other = (AbstractCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints);
    }


}