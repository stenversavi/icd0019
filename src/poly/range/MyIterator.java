package poly.range;

import java.util.Iterator;

public class MyIterator implements Iterator<Integer> {
    private int end;
    private int start;
    public MyIterator(int start, int end) {
        this.end = end;
        this.start = start;
    }

    @Override
    public boolean hasNext() {
        return start <= end;
    }

    @Override
    public Integer next() {
        return start++;
    }
}
