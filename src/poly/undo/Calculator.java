package poly.undo;

import java.util.Stack;
import java.util.function.Function;

public class Calculator {
    private Stack<Function<Double, Double>> actions = new Stack<>();

    private double value;

    public void input(double value) {
        double stored = this.value;
        actions.push(input -> stored);
        this.value = value;
    }

    public void add(double addend) {
        actions.push(input -> input - addend);
        value += addend;

    }

    public void multiply(double multiplier) {
        actions.push(input -> input / multiplier);
        value *= multiplier;
    }

    public double getResult() {
        return value;
    }

    public void undo() {
        Function<Double, Double> function = actions.pop();
        value = function.apply(value);
    }
}
