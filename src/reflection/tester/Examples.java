package reflection.tester;

import java.util.Arrays;

public class Examples {

    public static void main(String[] args) throws Exception {


        Class<?> aClass = Class.forName("reflection.tester.ExampleTests2");

        System.out.println(Arrays.toString(aClass.getMethods()));

        Class<? extends Throwable> expected = IllegalStateException.class;
        // expected = IllegalArgumentException.class;
        System.out.println(expected);

        if (expected.isAssignableFrom(RuntimeException.class)) {
            System.out.println("ok");
        }
        System.out.println("ok");

    }

}
