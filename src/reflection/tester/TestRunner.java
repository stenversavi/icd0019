package reflection.tester;

import java.lang.reflect.Method;
import java.util.List;

public class TestRunner {


    private String finalString = "";

    public void runTests(List<String> testClassNames) {
        for (String testClassName : testClassNames) {
            try {
                Class<?> clazz = Class.forName(testClassName);
                Object instance = clazz.getDeclaredConstructor().newInstance();
                Method[] methods = clazz.getDeclaredMethods();
                for (Method method : methods) {
                    if (method.isAnnotationPresent(MyTest.class)) {
                        if (method.getAnnotation(MyTest.class).expected() == MyTest.None.class) {
                            expectedIsNone(method, instance);
                        }
                        else{
                            Class<? extends Throwable> expected = method.getAnnotation(MyTest.class).expected();
                            ifExceptionIsExpected(method, instance, expected);
                        }
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    private void expectedIsNone(Method method, Object instance) {
        try {
            method.invoke(instance);
            finalString += toFinalString(true, method);
        } catch (Exception e) {
            finalString += toFinalString(false, method);
        }
    }

    private void ifExceptionIsExpected(Method method,
                                       Object instance,
                                       Class<? extends Throwable> expected) {
        try {
            method.invoke(instance);
            finalString += toFinalString(false, method);
        }
        catch (Exception e) {
            if (e.equals(expected) || expected.isAssignableFrom(e.getCause().getClass())) {
                finalString += toFinalString(true, method);
            }
            else {
                finalString += toFinalString(false, method);
            }
        }
    }

    public String toFinalString(boolean state, Method method) {
        if (state) {
            return method.getName() + "()" + " - OK" + "\n";
        }
        return method.getName() + "()" + " - FAILED" + "\n";
    }

    public String getResult() {
        return finalString;
    }
}
