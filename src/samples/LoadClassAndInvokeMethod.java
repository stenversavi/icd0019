package samples;

import reflection.tester.MyTest;

import java.lang.reflect.Method;

public class LoadClassAndInvokeMethod {

    public static void main(String[] args) throws Exception {

        Class<?> clazz = Class.forName("reflection.tester.ExampleTests2");

        Method method = clazz.getDeclaredMethod("test3");
        System.out.println(method.getAnnotation(MyTest.class).expected());

        Object instance = clazz.getDeclaredConstructor().newInstance();

        method.invoke(instance);

        }
}
