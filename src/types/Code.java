package types;

import java.util.*;

public class Code {

    public static void main(String[] args) {

        String letters = "";

        System.out.println(mode(letters)); // 11
    }

    public static int sum(int[] numbers) {
        int totalSum = 0;
        for (int number: numbers) {
            totalSum += number;
        }
        return totalSum;
    }

    public static double average(int[] numbers) {
        double totalSum = 0;
        double lenght = numbers.length;

        for (int number: numbers) {
            totalSum += number;
        }
        return Double.valueOf(totalSum / lenght);
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0) {
            return null; }
        int zeroElement = integers[0];
        for (int number: integers) {
            if (number < zeroElement) {
                zeroElement = number; }
        }
        return zeroElement;
    }

    public static String asString(int[] elements) {
        String finalString = "";
        int elementCount = 0;
        for (int element: elements) {
            if (elementCount == elements.length - 1) {
                finalString += element;
                return finalString;
            }
            finalString += element + ", ";
            elementCount += 1;
        }
        return finalString;
    }

    public static Character mode(String input) {
        if (input.length() == 0) {
            return null; }
        HashMap<Character, Integer> mode = new HashMap<Character, Integer>();
        char[] characters = input.toCharArray();
        for (char c : characters) {
            if (!mode.containsKey(c)) {
                mode.put(c, 1);
            } else {
                mode.put(c, mode.get(c) + 1);
            }
        }
        String popularChar = "";
        int maxValueInMap = Collections.max(mode.values());
        for (Map.Entry<Character, Integer> entry : mode.entrySet()) {
            if (entry.getValue() == maxValueInMap) {
                popularChar = String.valueOf(entry.getKey());
                break;
            }
        }
        return popularChar.charAt(0);
        }

    public static String squareDigits(String s) {
        String squaredDigits = "";
        char[] characterString = s.toCharArray();
        for (char c : characterString) {
            if (Character.isDigit(c)) {
                int argument = Character.getNumericValue(c);
                squaredDigits += pow(argument, 2);
            } else {
                squaredDigits += c;
            }
        }
        return squaredDigits;
    }

    private static int pow(int arg, int power) {
        // Java has Math.pow() but this time write your own implementation.
        if (arg == 0) {
            return 1;
        }
        int result = 1;
        while (power != 0) {
            result = result * arg;
            power--;
        }
        return result;
    }

    public static int isolatedSquareCount() {
        boolean[][] matrix = getSampleMatrix();

        printMatrix(matrix);

        int isolatedCount = 0;

        // count isolates squares here

        return isolatedCount;
    }

    private static void printMatrix(boolean[][] matrix) {
        for (boolean[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    private static boolean[][] getSampleMatrix() {
        boolean[][] matrix = new boolean[10][10];

        Random r = new Random(5);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = r.nextInt(5) < 2;
            }
        }

        return matrix;
    }
}